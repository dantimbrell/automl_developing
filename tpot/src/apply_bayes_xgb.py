import numpy as np
import pandas as pd
from bayes_opt import BayesianOptimization
import xgboost as xgb
from sklearn.cross_validation import cross_val_score

def xgboostcv(X, y, max_depth,
              learning_rate,
              n_estimators,
              gamma,
              min_child_weight,
              max_delta_step,
              subsample,
              colsample_bytree,
              silent=True,
              nthread=-1):
    return cross_val_score(xgb.XGBClassifier(max_depth=int(max_depth),
                                             learning_rate=learning_rate,
                                             n_estimators=int(n_estimators),
                                             silent=silent,
                                             nthread=nthread,
                                             gamma=gamma,
                                             min_child_weight=min_child_weight,
                                             max_delta_step=max_delta_step,
                                             subsample=subsample,
                                             colsample_bytree=colsample_bytree),
                           X,
                           y,
                           "roc_auc",
                           cv=5).mean()


# Load data set and target values

def apply_xgboost():
	xgboostBO = BayesianOptimization(xgboostcv,
                                 {'max_depth': (5, 10),
                                  'learning_rate': (0.01, 0.3),
                                  'n_estimators': (50, 1000),
                                  'gamma': (1., 0.01),
                                  'min_child_weight': (2, 10),
                                  'max_delta_step': (0, 0.1),
                                  'subsample': (0.7, 0.8),
                                  'colsample_bytree' :(0.5, 0.99)
                                  })

	xgboostBO.maximize()



	print('XGBOOST: %f' % xgboostBO.res['max']['max_val'])
	return xgboostBO.res['max']['max_val']