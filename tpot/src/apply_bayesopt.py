import numpy as np
import pandas as pd
from bayes_opt import BayesianOptimization
import lightgbm as lgb
import datetime as dt
from lightgbm import LGBMClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.linear_model import RandomizedLogisticRegression
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

def bayes_parameter_opt_lgb(X, Y,init_round=50, opt_round=20, n_folds=5, random_seed=6, 
                            n_estimators=10000, learning_rate=0.05, output_process=False):
    print('splitting')
    Xtr, Xv, ytr, yv = train_test_split(X, Y, test_size=0.2, random_state=1990)
    print('split')
    # parameters
    def lgb_cv(num_leaves, feature_fraction, bagging_fraction, max_depth, 
               lambda_l1, lambda_l2, min_split_gain, min_child_weight):
        
        params = {'application':'binary','num_iterations': n_estimators, 'learning_rate':learning_rate, 
                  'early_stopping_round':100, 'metric':'auc'}
        params["num_leaves"] = int(round(num_leaves))
        params['feature_fraction'] = max(min(feature_fraction, 1), 0)
        params['bagging_fraction'] = max(min(bagging_fraction, 1), 0)
        params['max_depth'] = int(round(max_depth))
        params['lambda_l1'] = max(lambda_l1, 0)
        params['lambda_l2'] = max(lambda_l2, 0)
        params['min_split_gain'] = min_split_gain
        params['min_child_weight'] = min_child_weight
        cv_result = lgb.cv(params, train_data, nfold=n_folds, seed=random_seed, stratified=True, verbose_eval =200, metrics=['auc'])
        return max(cv_result['auc-mean'])
    # range 
    def lgb_c1(num_leaves, colsample_bytree, subsample, reg_alpha, reg_lambda, 
               min_child_weight, min_split_gain, min_child_samples):
        params = {'n_estimators': n_estimators, 'learning_rate':learning_rate, 'early_stopping_round':100, 'metric':'auc'}
        params["num_leaves"]=int(round(num_leaves))
        params["colsample_bytree"]=max(colsample_bytree, 0)
        params["subsample"]=max(subsample,0)
        params["reg_alpha"]=max(reg_alpha,0)
        params["reg_lambda"]=max(reg_lambda,0)
        params["n_thread"]=4
        params["min_split_gain"]=max(0, min_split_gain)
        params["min_child_weight"]=max(min_child_weight, 0)
        params["min_child_samples"]=int(max(0, min_child_samples))
        lgb_model=LGBMClassifier(n_thread=params["n_thread"],
                       n_estimators=params["n_estimators"],
                       learning_rate=params["learning_rate"],
                       num_leaves=params["num_leaves"],
                       colsample_bytree=params["colsample_bytree"],
                       subsample=params["subsample"],
                       reg_alpha=params["reg_alpha"],
                       reg_lambda=params["reg_lambda"],
                       min_split_gain = params["min_split_gain"],
                       min_child_samples=params["min_child_samples"],
                       min_child_weight=params['min_child_weight'],
                       silent=-1,
                       verbose=2)
        lgb_model.fit(Xtr, ytr, eval_set=[(Xtr, ytr), (Xv, yv)], 
                      eval_metric= 'auc', verbose= 100, early_stopping_rounds= 200)
        score = lgb_model.best_score_['valid_1']['auc']
        return score
    lgbBO = BayesianOptimization(lgb_c1, {'num_leaves': (2, 10000),
                                            'colsample_bytree': (0.01, 0.99),
                                            'subsample': (0.01, 0.99),
                                            #'bagging_fraction': (0.8, 1),
                                            #'max_depth': (5, 12),
                                            'reg_alpha': (0.01, 10),
                                            'reg_lambda': (0.01, 10),
                                            'min_split_gain': (0.001, 0.1),
                                            'min_child_samples':(1, 2000),
                                            'min_child_weight': (1, 1000)}, random_state=0)
    #lgbBO = BayesianOptimization(lgb_cv, {'num_leaves': (5, 200),
    #                                        'feature_fraction': (0.1, 0.9),
    #                                        'bagging_fraction': (0.2, 1),
    #                                        'max_depth': (5, 12),
    #                                        'lambda_l1': (0, 5),
    #                                        'lambda_l2': (0, 3),
    #                                        'min_split_gain': (0.001, 0.1),
    #                                        'min_child_weight': (5, 100)}, random_state=0)
    # optimize
    t0 = dt.datetime.now()
    lgbBO.maximize(init_points=init_round, n_iter=opt_round)
    t1 = dt.datetime.now()
    # output optimization process
    if output_process==True: lgbBO.points_to_csv("bayes_opt_result.csv")
    time_ = (t1-t0).seconds
    print(time_)
    print(lgbBO.res['max']['max_val'])
    score_ = lgbBO.res['max']['max_val']
    d = {'FUNCTION_NAME': ['Bayesopt_lgbmclassifier_init%s_opt%s' % (init_round, opt_round)],'SCORE':[score_],'TIME':[time_]}
    export = pd.DataFrame(d)
    export.to_csv('../exported_files/times_exported_bayes_lgb_init%s_opt%s.csv' %(init_round, opt_round))
    print(export)
    # return best parameters
    return lgbBO.res['max']['max_params']
    
print('everything loaded and defined')