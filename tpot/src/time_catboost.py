import catboost as cb
from sklearn.utils import class_weight
import numpy as np
import pandas as pd
from bayes_opt import BayesianOptimization
import datetime as dt
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
def load_sample_data(): 
    """
    Read sample credit card default data to  pandas dataframe and separate the target column which is 'default'
    Read DataSet_Description.html for more information.
    """
    data = pd.read_csv("../../data/UCI_Credit_Card.csv")
    X = data.drop("default.payment.next.month",axis=1)
    y = data.pop('default.payment.next.month')
    #Y = data["default.payment.next.month"]
    return X,y

def apply_cb_classifier(X,y, n_iterations=1000, Bayes=False, init_round=1, opt_round=1):
    """
    Applies Catboost to a data set, with an otional Bayesian Hyperparameter Optimization.
    params:
        X : the training set
        y: the target of the training set
        n_iterations: the number of iterations for the CatboostClassifier
        Bayes: Boolean. If True, applies Bayesian Optimization
        init_round: Only applicable if Bayes=True. The number of initial hyperparameter points
        opt_round: Only applicable if Bayes=False. The number of hyperparameter optimizations
        
    Returns: a small csv file with the following format:
        
        Function Name (Catboost with/without Bayesian) | Score | Time taken
    
    """
    Xtr, Xv, ytr, yv = train_test_split(X, y, test_size=0.25, random_state=1990)
    t0 = dt.datetime.now()
    col_types=np.asarray(Xtr.dtypes,dtype='str') 
    categorical_features_indices = np.where(col_types=='object')[0]
    class_weights = class_weight.compute_class_weight('balanced',np.unique(y),y) 
    
    
    if Bayes==True:
        def cb_c1(depth, l2_leaf_reg, rsm, border_count, model_size_reg):
            params = {'iterations': int(n_iterations), 'od_wait':100, 'eval_metric':'AUC'}
            params["depth"] = int(max(2,depth))
            params['l2_leaf_reg']=max(0.1, l2_leaf_reg)
            params["learning_rate"]=0.1
            params['rsm']=max(0.1, rsm)
            params['border_count']=int(max(1, border_count))
            params['model_size_reg'] = max(model_size_reg,0)
            params['verbose']=500
            params['class_weights']=class_weights
            cb_model = cb.CatBoostClassifier(iterations=params['iterations'],
                                             od_wait = params['od_wait'],
                                             eval_metric=params['eval_metric'],
                                             depth=params['depth'],
                                             l2_leaf_reg=params['l2_leaf_reg'],
                                            # learning_rate=params['learning_rate'],
                                             rsm = params['rsm'],
                                             border_count=params['border_count'],
                                             model_size_reg=params['model_size_reg'],
                                             verbose = params['verbose'],
                                             class_weights = params['class_weights'],
                                             random_seed=1990
                                            # feature_border_type=None,
                                             #old_permutation_block_size=None,
                                             #od_pval=None,
                                             #od_type=None,
                                             #nan_mode=None,
                                             #counter_calc_method=None,
                                             #leaf_estimation_iterations=None,
                                             #leaf_estimation_method=None,
                                             #thread_count=None,
                                             #random_seed=None,
                                             #use_best_model=None,
                                             #verbose=None,
                                             #logging_level=None,
                                             #metric_period=None,
                                             #ctr_leaf_count_limit=None,
                                             #store_all_simple_ctr=None,
                                             #max_ctr_complexity=None,
                                             #has_time=None,
                                             #allow_const_label=None,
                                             #classes_count=None,
                                             #class_weights=None,
                                             #one_hot_max_size=None,
                                             #random_strength=None,
                                             #name=None,
                                             #ignored_features=None,
                                             #train_dir=None,
                                             #custom_loss=None,
                                             ##custom_metric=None,
                                             #eval_metric=None,
                                             #bagging_temperature=None,
                                             #save_snapshot=None,
                                             #snapshot_file=None,
                                             #fold_len_multiplier=None,
                                             #used_ram_limit=None,
                                             #gpu_ram_part=None,
                                             #allow_writing_files=None,
                                             #final_ctr_computation_mode=None,
                                             #approx_on_full_history=None,
                                             #boosting_type=None,
                                             #simple_ctr=None,
                                             #combinations_ctr=None,
                                             #per_feature_ctr=None,
                                             #task_type=None,
                                             #device_config=None,
                                             #devices=None,
                                             #bootstrap_type=None,
                                             #subsample=None,
                                             #max_depth=None,
                                             #n_estimators=None,
                                             #num_boost_round=None,
                                             #num_trees=None,
                                             #colsample_bylevel=None,
                                             #random_state=None,
                                             #reg_lambda=None,
                                             #objective=None,
                                             #eta=None,
                                             #max_bin=None,
                                             #scale_pos_weight=None,
                                             #gpu_cat_features_storage=None,
                                             #data_partition=None
                                             #metadata=None
                                 )
            cb_model.fit(Xtr, ytr, eval_set=(Xv, yv), cat_features= categorical_features_indices,use_best_model=True, verbose=True)
       # score = cb_model.bestscore()
            preds_ = np.round(cb_model.predict_proba(Xv)[:,1],1)
            print('Full AUC score %.6f' % roc_auc_score(yv, preds_))
            score=roc_auc_score(yv, preds_)
            return score
        

        cbBO = BayesianOptimization(cb_c1, {"depth": (2, 10),
                                                        'l2_leaf_reg':(0.1, 10),
                                                        #"learning_rate":(0.002, 0.3),
                                                        'rsm':(0.1, 1),
                                                        'border_count':(1, 255),
                                                        'model_size_reg':(0,10)}, 
                                                         random_state=0)
    #lgbBO = BayesianOptimization(lgb_eval, {'num_leaves': (5, 80),
    #                                        'feature_fraction': (0.1, 0.9),
    #                                        'bagging_fraction': (0.8, 1),
    #                                        'max_depth': (5, 12),
    #                                        'lambda_l1': (0, 5),
    #                                        'lambda_l2': (0, 3),
    #                                        'min_split_gain': (0.001, 0.1),
    #                                        'min_child_weight': (5, 100)}, random_state=0)
    # optimize
        cbBO.maximize(init_points=init_round, n_iter=opt_round)
    
       
        
    # output optimization process
    #if output_process==True: cbBO.points_to_csv("bayes_opt_result_cb.csv")
    
    # return best parameters
        bayes_params=cbBO.res['max']['max_params']
        print(bayes_params)
        #bayes_params['iterations']= int(n_iterations)
        bayes_depth=int(bayes_params['depth'])
        bayes_l2l = bayes_params['l2_leaf_reg']
        bayes_rsm = bayes_params['rsm']
        bayes_bc=int(bayes_params['border_count'])
        bayes_msr = bayes_params['model_size_reg']
        cb_model = cb.CatBoostClassifier(iterations=n_iterations, 
                                         depth = bayes_depth,
                                         l2_leaf_reg = bayes_l2l,
                                         rsm = bayes_rsm,
                                         border_count = bayes_bc,
                                         model_size_reg = bayes_msr,
                                         eval_metric='AUC',
                                         od_wait=100
                                         )
        
    else:
    
        cb_model = cb.CatBoostClassifier(iterations=n_iterations, learning_rate=0.1,od_wait=100, eval_metric='AUC',class_weights=class_weights)
    cb_model.fit(Xtr, ytr, eval_set=(Xv, yv), cat_features= categorical_features_indices,use_best_model=True, verbose=False)
    preds_=cb_model.predict_proba(Xv)[:,1]
    score_=roc_auc_score(yv, preds_)
    t1 = dt.datetime.now()
    time_ = (t1-t0).seconds
    time_ = str(time_)
    if Bayes==True:
        name = 'CATBOOST_BAYES'
    else:
        name = 'CATBOOST'
    d={'FUNCTION_NAME': [name],'Score': [score_], 'Time': [time_]}
    all_data = pd.DataFrame(data=d)
    print(all_data)
    if Bayes==True:
        all_data.to_csv('../exported_files/CATBOOST_BAYESIAN_TIMED_%s_init_%s_opt.csv' %(init_round, opt_round), index=False)
    else:
        all_data.to_csv('../exported_files/CATBOOST_TIMED.csv', index=False)
    
    return all_data

#X,y=load_sample_data()
#bo=apply_cb_classifier(X,y, n_iterations=1000, Bayes=True, init_round=20, opt_round=20)