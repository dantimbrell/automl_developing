import pandas as pd
from tpot import TPOTClassifier
from sklearn.model_selection import train_test_split
import numpy as np
import pandas as pd
from tpot import TPOTClassifier
from sklearn.model_selection import train_test_split
import datetime as dt

def tpot_dictionary():
    """
    Returns the default tpot classification dictionary according to the documentation:
    https://github.com/EpistasisLab/tpot/blob/master/tpot/config/classifier.py
    """
    classifier_config_dict = {

    # Classifiers
    'sklearn.naive_bayes.GaussianNB': {
    },

    'sklearn.naive_bayes.BernoulliNB': {
        'alpha': [1e-3, 1e-2, 1e-1, 1., 10., 100.],
        'fit_prior': [True, False]
    },

   

    'sklearn.tree.DecisionTreeClassifier': {
        'criterion': ["gini", "entropy"],
        'max_depth': range(1, 11),
        'min_samples_split': range(2, 21),
        'min_samples_leaf': range(1, 21)
    },

    'sklearn.ensemble.ExtraTreesClassifier': {
        'n_estimators': [100],
        'criterion': ["gini", "entropy"],
        'max_features': np.arange(0.05, 1.01, 0.05),
        'min_samples_split': range(2, 21),
        'min_samples_leaf': range(1, 21),
        'bootstrap': [True, False]
    },

    'sklearn.ensemble.RandomForestClassifier': {
        'n_estimators': [100],
        'criterion': ["gini", "entropy"],
        'max_features': np.arange(0.05, 1.01, 0.05),
        'min_samples_split': range(2, 21),
        'min_samples_leaf':  range(1, 21),
        'bootstrap': [True, False]
    },

    'sklearn.ensemble.GradientBoostingClassifier': {
        'n_estimators': [100],
        'learning_rate': [1e-3, 1e-2, 1e-1, 0.5, 1.],
        'max_depth': range(1, 11),
        'min_samples_split': range(2, 21),
        'min_samples_leaf': range(1, 21),
        'subsample': np.arange(0.05, 1.01, 0.05),
        'max_features': np.arange(0.05, 1.01, 0.05)
    },

    'sklearn.neighbors.KNeighborsClassifier': {
        'n_neighbors': range(1, 101),
        'weights': ["uniform", "distance"],
        'p': [1, 2]
    },

    'sklearn.svm.LinearSVC': {
        'penalty': ["l1", "l2"],
        'loss': ["hinge", "squared_hinge"],
        'dual': [True, False],
        'tol': [1e-5, 1e-4, 1e-3, 1e-2, 1e-1],
        'C': [1e-4, 1e-3, 1e-2, 1e-1, 0.5, 1., 5., 10., 15., 20., 25.]
    },
    }
    return classifier_config_dict

def test_tpot_dictionary():
    """
    Function to be used as a tpot_dictionary() with few classifiers for testing purposes.
    Choosing same classifiers as the one in ziping
    """
    classifier_config_dict = {

    # Classifiers
      'sklearn.ensemble.RandomForestClassifier': {
      'n_estimators': [100],
      'criterion': ["gini", "entropy"],
      'max_features': np.arange(0.05, 1.01, 0.05),
      'min_samples_split': range(2, 21),
      'min_samples_leaf':  range(1, 21),
      'bootstrap': [True, False]
     },
      'sklearn.ensemble.ExtraTreesClassifier': {      
    },
      'sklearn.ensemble.GradientBoostingClassifier': {
    'n_estimators': [100]
     },

    'sklearn.naive_bayes.BernoulliNB': {
        'alpha': [1e-3, 1e-2, 1e-1, 1., 10., 100.],
        'fit_prior': [True, False]
    },
    }
    return classifier_config_dict

def apply_tpot_classifier(X, y,gen, pop, cv=2, test_size=0.25, scoring='roc_auc'):
    """
    Applies the TPOTClassifier to the training data and prints the score of the model on the validation set. 
    The best model is then exported as 'apply_tpot_classifier_exported.py'.
    params:
        X: training set WITHOUT the target variable to predict.
        y: the target to predict.
        gen: the number of generations that the TPOT algorithm considers.
        pop: the population size that the TPOT algorithm considers.
        cv: the number of cross-validation folds. Default = 2.
        test_size: the percentage of the training set to be split into the validation set used for scoring. Default = 0.1.
        scoring: the metric to be used to score in the TPOT algorithm. Default = 'roc_auc'.
    """
    
    X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=test_size, random_state=1990)
    
    
    tpot = TPOTClassifier(generations=gen, population_size=pop, cv=cv, verbosity=2, 
                          scoring=scoring)
    tpot.fit(X_train, y_train)
    print(tpot.score(X_valid, y_valid))
    tpot.export('apply_tpot_classifier_exported.py')
    
    return classifier_config_dict

def time_apply_tpot_classifier(X, y,gen, pop, cv=2, test_size=0.25, scoring='roc_auc'):
    """
    Applies the TPOTClassifier to the training data and prints the score of the model on the validation set. 
    The best model is then exported as 'apply_tpot_classifier_exported.py'.
    params:
        X: training set WITHOUT the target variable to predict.
        y: the target to predict.
        gen: the number of generations that the TPOT algorithm considers.
        pop: the population size that the TPOT algorithm considers.
        cv: the number of cross-validation folds. Default = 2.
        test_size: the percentage of the training set to be split into the validation set used for scoring. Default = 0.1.
        scoring: the metric to be used to score in the TPOT algorithm. Default = 'roc_auc'.
    """
    
    X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=test_size, random_state=1990)
    
    t0 = dt.datetime.now()
    tpot = TPOTClassifier(generations=gen, population_size=pop, cv=cv, verbosity=2, 
                          scoring=scoring)
    tpot.fit(X_train, y_train)
    t1 = dt.datetime.now()
    time_ = (t1-t0).seconds
    print(tpot.score(X_valid, y_valid))
    tpot.export('apply_tpot_classifier_exported.py')
    print('Time taken is %d seconds' %(time_))


def time_each_apply_tpot_classifier(X, y, gen, pop, cv=2, test_size=0.25,name_of_export ='times_exported', scoring='roc_auc', c_dict = test_tpot_dictionary()):
    """
    Times how long each function in the TPOTClassifier takes to fit to the training data and saves the time and score 
    and name of the functions used.
    
    NOTE: Multinomial not used due to regression/classification clash of error given. This is the only function REMOVED
    from the TPOTClassifier standard dict.
    params:
        X: training set WITHOUT the target variable to predict.
        y: the target to predict.
        gen: the number of generations that the TPOT algorithm considers.
        pop: the population size that the TPOT algorithm considers.
        cv: the number of cross-validation folds. Default = 2.
		name_of_export: the filename prefix that is used for the exportation of files
        test_size: the percentage of the training set to be split into the validation set used for scoring. Default = 0.1.
        scoring: the metric to be used to score in the TPOT algorithm. Default = 'roc_auc'.
        name_of_export: the name of the exported .csv file.
		c_dict: the dictionary of models to be used by TPOT. We choose here the test_tpot_dictionary() function from above as the default. 
        
    Exports:
        A .csv file that is structured:
        
        |NAME_OF_FUNCTION | SCORE | TIME|
        ---------------------------------
        
    Returns:
        The pandas dataframe with the above export structure.
    """
    
    X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=test_size, random_state=1990)
    name = []
    score = []
    time = []
    tpot_config = c_dict
    
    for item in tpot_config:
        
        new_dict = {item: tpot_config[item]}
        print(new_dict)
        t0 = dt.datetime.now()
        
        tpot = TPOTClassifier(generations=gen, population_size=pop, cv=cv, verbosity=2, 
                          scoring=scoring, config_dict=new_dict)
        tpot.fit(pd.np.array(X_train), pd.np.array(y_train).ravel())
        score_=(tpot.score(X_valid, y_valid))
        t1 = dt.datetime.now()
        time_ = (t1-t0).seconds
        name.append(item)
        time.append(time_)
        score.append(score_)
        time_ = str(time_)
        
    d = {'FUNCTION_NAME' : name, 'SCORE': score, 'TIME':time}
    all_data = pd.DataFrame(data=d)
    all_data.to_csv('../exported_files/%s.csv' %(name_of_export), index=False)
    return all_data
