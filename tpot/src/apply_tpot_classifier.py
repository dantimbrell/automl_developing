import pandas as pd
from tpot import TPOTClassifier
from sklearn.model_selection import train_test_split


def load_sample_data(): 
    """
    Read sample credit card default data to  pandas dataframe and separate the target column which is 'default'
    Read DataSet_Description.html for more information.
    """
    data = pd.read_csv("../data/UCI_Credit_Card.csv")
    data.rename(columns={'default.payment.next.month': 'class'}, inplace=True)
    X = data.drop("class",axis=1)
    y = data["class"]
    return X,y
    

def apply_tpot_classifier(X, y,gen, pop, cv=2, test_size=0.25, scoring='roc_auc'):
    """
    Applies the TPOTClassifier to the trainng data and prints the score of the model on the validation set. 
    The best model is then exported as 'apply_tpot_classifier_exported.py'.
    params:
        X: training set WITHOUT the target variable to predict.
        y: the target to predict.
        gen: the number of generations that the TPOT algorithm considers.
        pop: the population size that the TPOT algorithm considers.
        cv: the number of cross-validation folds. Default = 2.
        test_size: the percentage of the training set to be split into the validation set used for scoring. Default = 0.1.
        scoring: the metric to be used to score in the TPOT algorithm. Default = 'roc_auc'.
    """
    
    X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=test_size, random_state=1990)
    tpot = TPOTClassifier(generations=gen, population_size=pop, cv=cv, verbosity=2, scoring=scoring)
    tpot.fit(X_train, y_train)
    print(tpot.score(X_valid, y_valid))
    tpot.export('../exported_files/apply_tpot_classifier_exported.py')


def apply_tpot_regressor(X, y,generation, pop, cv=2, test_size=0.25):
    """
    Function to apply tpot regression
    """
    
    X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=test_size, random_state=1989)
    tpot = TPOTRegressor(generations=generation, population_size=pop, verbosity=2) #recomended pop = 50
    tpot.fit(X_train, y_train)
    print(tpot.score(X_test, y_test))
    tpot.export('../exported_files/apply_tpot_regression.py')


def get_tpot_dict(pipeline, classifier):
    """
    Function that returns a dictionary with the parameters of the classifier specifeied
    """
    my_dict = {k: v for k, v in pipeline if classifier in k}
    return my_dict
