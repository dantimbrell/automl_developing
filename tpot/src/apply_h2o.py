def apply_h2o(df, max_runtime_secs=30):
    """
    Initiates h2o, splits the data into training and validation and test
    """
    import h2o
    import pandas as pd
    from h2o.automl import H2OAutoML
    from sklearn.metrics import roc_auc_score
    h2o.init()
    data = h2o.H2OFrame(df)
    
    x = data.columns
    y = "default.payment.next.month"
    x.remove(y)
    data[y] = data[y].asfactor()
    train,test,valid = data.split_frame(ratios=[0.75, 0.15])


    aml = H2OAutoML(max_runtime_secs = max_runtime_secs)
    aml.train(x = x, y = y,
          training_frame = train,
          validation_frame = valid)

# View the AutoML Leaderboard
    lb = aml.leaderboard
    print(lb)
    aml.leader
    print(aml.leader.auc(valid=True))
    validation_score=aml.leader.auc(valid=True)
    preds = aml.leader.predict(valid)
    h2o.cluster().shutdown(prompt=True)
    return validation_score