#write a function which compares the scores between different algorithms used by TPOT and save them into a csv file
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def read_zip_results(filename):
    """
    Function for a quick load of a csv file
    """
    df = pd.read_csv(filename)
    #df = df.rename(index=str, columns={"Test-Score": "SCORE", "Time": "TIME"})
    return df

def simple_concat():
    """
    Function to quick concatinate results with the scores 
    """
    result = pd.concat([df_scores, df_bayesian, df_bayesian_lbm], ignore_index=False, sort = 'True')
    result = result.sort_index(axis = 0)
    return result

def join_path(file_name, path=default_path):
    """
     Function to join paths and name of a file
     param: 
     file_name: file name 
     path: path to the file name
     
     return:
     full path + filename
     """
    return os.path.join(path, file_name)

def load_score(file_name, path=default_path):
    """
    Function to load the scores from one file and sorts them based on the performance
    param: 
    file_name: file name
    path_name: path to the filename

    return:
    sorted algorithm scores
    """
    full_path = join_path(file_name, path=path)
    return pd.read_csv(full_path, delimiter=',').sort_values(by=['SCORE'])

def load_scores(file_names, path=default_path):
    """
    Function to load the scores from different algorithms and sorts them based
    on the performance
    param:
    file_names: a list of files
    path: path to the file

    return:
    sorted algorithm scores
    """
    #return list(map(load_score, file_names))
    return list(load_score(x) for x in file_names)

def scores_plot(df, size = 10):
    """
    Function to plot the scores of each classifier used 
    """
    
    ax1 = df.plot.bar(x='FUNCTION_NAME', y='SCORE', rot=90, stacked=True, width=0.3, align='center', alpha = 0.5)
    ax2 = df.plot.bar(x='FUNCTION_NAME', y='TIME', rot=90, stacked=True, width=0.3, align='center', alpha = 0.5)
    
    ax1.grid(zorder=0)
    ax2.grid(zorder=0)
    
    ax1 = plt.gca()
    ax1 = plt.gca()

    ax1.set_xticklabels(range(len(df)))
    ax2.set_xticklabels(range(len(df)))
    
    ax1.plot()
    ax2.plot()

